import java.util.Scanner;

// Sonar lint wants to move this file to a named package.
public class Main {

    public static void main(String[] args) {
        Triangle trinagle = new Triangle();
        Scanner scan = new Scanner(System.in);

        // According to Sonar lint i should use system out with loggers however i find this unnecessary at the moment
        System.out.println("Choose a number for the sideA:");
        int sideA = scan.nextInt();
        System.out.println("Choose a number for the sideB:");
        int sideB = scan.nextInt();
        System.out.println("Choose a number for the sideC:");
        int sideC = scan.nextInt();
        trinagle.calculate(sideA, sideB, sideC);
    }
}
